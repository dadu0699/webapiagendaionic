var database = require("../config/database.config");
var Cita = {};

Cita.selectAll = function(callback) {
  if(database) {
		database.query('SELECT * FROM Cita',
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Cita.select = function(idUsuario, callback) {
  if(database) {
		database.query('SELECT * FROM cita_usuario WHERE idUsuario = ?', idUsuario,
     function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Cita.insert = function(data, callback) {
  if(database) {
    database.query('CALL agregar_cita(?, ?, ?, ?)',
    [data.idContacto, data.titulo, data.lugar, data.fecha],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Cita.update = function(data, callback){
	if(database) {
		database.query('CALL modificar_cita(?, ?, ?, ?, ?)',
    [data.idCita, data.idContacto, data.titulo, data.lugar, data.fecha],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Cita.delete = function(idCita, callback) {
	if(database) {
		database.query('CALL eliminar_cita(?)', idCita,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Cita;
