var database = require("../config/database.config");
var Estado = {};

Estado.select = function(callback) {
  if(database) {
		database.query('SELECT * FROM Estado', function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

module.exports = Estado;
