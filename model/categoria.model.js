var database = require("../config/database.config");
var Categoria = {};

Categoria.selectAll = function(callback) {
  if(database) {
		database.query("SELECT * FROM Categoria WHERE nombreCategoria!='Sin Categoria'",
    function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(resultados);
			}
		});
	}
}

Categoria.insert = function(data, callback) {
  if(database) {
    database.query('INSERT INTO Categoria SET ?', data, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Categoria.delete = function(idCategoria, callback) {
	if(database) {
		database.query('CALL eliminar_categoria(?)', idCategoria,
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Categoria;
