var express = require('express');
var cita = require('../../model/cita.model');
var services = require('../../services');
var router = express.Router();

router.get('/cita/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  cita.select(idUsuario, function(citas) {
    if(typeof citas !== 'undefined') {
      res.json(citas);
    } else {
      res.json({"mensaje" : "No hay citas"});
    }
  });
});

router.get('/cita/:id', services.verificar, function(req, res, next) {
  var idCita = req.params.id;
  var idUsuario = req.usuario.idUsuario;
  cita.select(idUsuario, function(citas) {
    if(typeof citas !== 'undefined') {
      res.json(citas.find(c => c.idCita == idCita));
    } else {
      res.json({"mensaje" : "No hay citas"});
    }
  });
});

router.post('/cita', services.verificar, function(req, res, next) {
  var data = {
    idContacto : req.body.idContacto,
    titulo : req.body.titulo,
    lugar : req.body.lugar,
    fecha : req.body.fecha
  };

  cita.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego la cita"
      });
    } else {
      res.json({"mensaje":"No se ingreso la cita"});
    }
  });
});

router.put('/cita/:idCita', function(req, res, next){
  var idCita = req.params.idCita;
  var data = {
    idContacto : req.body.idContacto,
    titulo : req.body.titulo,
    lugar : req.body.lugar,
    fecha : req.body.fecha,
    idCita : idCita
  }
  cita.update(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    } else {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    }
  });
});

router.delete('/cita/:idCita', function(req, res, next){
  var idCitaUri = req.params.idCita;
  cita.delete(idCitaUri, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino la cita correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino la cita"});
    }
  });
});

module.exports = router;
