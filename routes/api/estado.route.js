var express = require('express');
var router = express.Router();
var estado = require('../../model/estado.model');

router.get('/estado/', function(req, res, next) {
  estado.select(function(estados) {
    if(typeof estados !== 'undefined') {
      res.json(estados);
    } else {
      res.json({"mensaje" : "No hay estados"});
    }
  });
});

module.exports = router;
