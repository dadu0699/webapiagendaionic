var express = require('express');
var tarea = require('../../model/tarea.model');
var services = require('../../services');
var router = express.Router();

router.get('/tarea/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
  tarea.select(idUsuario, function(tareas) {
    if(typeof tareas !== 'undefined') {
      res.json(tareas);
    } else {
      res.json({"mensaje" : "No hay tareas"});
    }
  });
});

router.get('/tarea/:id', services.verificar, function(req, res, next) {
  var idTarea = req.params.id;
  var idUsuario = req.usuario.idUsuario;
  tarea.select(idUsuario, function(tareas) {
    if(typeof tareas !== 'undefined') {
      res.json(tareas.find(c => c.idTarea == idTarea));
    } else {
      res.json({"mensaje" : "No hay tareas"});
    }
  });
});

router.post('/tarea', services.verificar, function(req, res, next) {
  var data = {
    idUsuario: req.usuario.idUsuario,
    idEstado : req.body.idEstado,
    titulo : req.body.titulo,
    descripcion : req.body.descripcion,
    fechaFin : req.body.fechaFin
  };

  tarea.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego la tarea"
      });
    } else {
      res.json({"mensaje":"No se ingreso la tarea"});
    }
  });
});

router.put('/tarea/:idTarea', function(req, res, next){
  var idTarea = req.params.idTarea;
  var data = {
    idEstado : req.body.idEstado,
    titulo : req.body.titulo,
    descripcion : req.body.descripcion,
    fechaFin : req.body.fechaFin,
    idTarea : idTarea
  }
  tarea.update(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    } else {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    }
  });
});

router.delete('/tarea/:idTarea', function(req, res, next){
  var idTareaUri = req.params.idTarea;
  tarea.delete(idTareaUri, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino la tarea correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino la tarea"});
    }
  });
});

module.exports = router;
