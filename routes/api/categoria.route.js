var express = require('express');
var categoria = require('../../model/categoria.model');
var router = express.Router();

router.get('/categoria/', function(req, res, next) {
  categoria.selectAll(function(categorias) {
    if(typeof categorias !== 'undefined') {
      res.json(categorias);
    } else {
      res.json({"mensaje" : "No hay categorias"});
    }
  });
});

router.post('/categoria', function(req, res, next) {
  var data = {
    idCategoria : null,
    nombreCategoria : req.body.nombreCategoria
  }

  categoria.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego la categoria"
      });
    } else {
      res.json({"mensaje":"No se ingreso la categoria"});
    }
  });
});

router.delete('/categoria/:idCategoria', function(req, res, next){
  var idCategoriaUri = req.params.idCategoria;
  categoria.delete(idCategoriaUri, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino el contacto correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino el contacto"});
    }
  });
});

module.exports = router;
